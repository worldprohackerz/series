import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  foundnumber;
  title = 'series';
  series = []
  constructor() {
    this.series = [2,3,10,15,26,35,50,63,82]
  }
  findnumber() {
    if(this.series.length >= this.foundnumber) {
      this.foundnumber = this.series[this.foundnumber];
    }
  }
}
